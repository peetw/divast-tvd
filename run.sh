#!/bin/sh

viewHelp () {
cat << EOF
# -----------------------------------------------------------------------------
# Wrapper script for divast-tvd that handles post-processing such as
# result compression and syncronization.
#
#   Usage:
#       $0 [OPTIONS] FILES...
#
#   Options:
#       -o [DIR]  Move compressed data to DIR (default:
#                   ~/workspace/divast-tvd-visualize/Wigborough)
#       -e        Copy compressed data to Dropbox
#       -s        Put PC into sleep mode once complete
#       -f        Put PC into hibernate mode once complete
#
#   Where:
#       Each FILE is a divast-tvd input file
# -----------------------------------------------------------------------------
EOF
[ "$1" ] && echo "\n  *** $1 ***\n"
exit 1
}

# Default values
DIR="/home/peet/workspace/divast-tvd"
BIN="$DIR/bin/divast-tvd"
OUTDIR="$DIR/out"
INPDIR="$DIR/out/inp"
DROPBOX="/home/peet/Dropbox/results"
STORAGE="/home/peet/workspace/divast-tvd-visualize/Wigborough"

# Process command line options
while getopts ":o:esfh" opt; do
	case $opt in
		o)
			STORAGE="$OPTARG"
			;;
		e)
			EXPORT=1
			;;
		s)
			SLEEP=1
			;;
		f)
			HIBERNATE=1
			;;
		h)
			viewHelp
			;;
		\?)
			viewHelp "Invalid option: -$OPTARG"
			;;
		:)
			viewHelp "Option -$OPTARG requires an argument"
			;;
	esac
done

# Remove options from argument list and check for args
shift $((OPTIND-1))
if [ "$#" -lt 1 ]; then
	viewHelp "Please specify at least one input file"
fi

# Ensure output directory exists
mkdir -p "$OUTDIR"

# Loop over each input file
for input in "$@"; do
	# Run model
	$BIN "$input" || FAIL=1

	# Post process if successful
	if [ ! "$FAIL" ]; then
		# Copy input files
		mkdir -p "$INPDIR" || viewHelp "Could not create directory \"$INPDIR\""
		rm -f "$INPDIR/"*
		while read line; do
			line=$(echo "$line" | sed -e "s/'//g")
			cp "$line" "$INPDIR"
		done < "$input"

		# Get output filenames
		name=$(basename "$input" | sed 's/\.dat//')
		output_domain="${name}_UVE.dat"
		output_monitor="${name}_LST.dat"

		# Compress data
		cd "$OUTDIR"
		tarfile=$(date +%F_%H.%M)"_${name}.tar"
		tar -cf "$tarfile" "inp/" "$output_domain" "$output_monitor"
		archive="${tarfile}.lrz"
		lrzip -q -o "$archive" "$tarfile"

		# Clean up
		if [ "$EXPORT" ]; then
			cp "$archive" "$DROPBOX"
		fi
		mv "$archive" "$STORAGE"
		rm "$tarfile" "$output_domain" "$output_monitor"
		[ "$INPDIR" ] && [ "$INPDIR" != "/" ] && rm -rf "$INPDIR"
		cd "$DIR"
	else
		echo "ERROR ("$(date +%F_%H.%M)"): divast-tvd FAILED TO RUN $input"
	fi

	# Reset failure flag
	FAIL=""
done

# Put PC into sleep mode
if [ "$SLEEP" ]; then
	sudo pm-suspend
elif [ "$HIBERNATE" ]; then
	sudo pm-hibernate
fi
