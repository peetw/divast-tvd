MODULE OUTPUT

    IMPLICIT NONE

    INTEGER :: NMONIT, MSEC_START, MSEC_STOP, NUMTSTEP = 0, NUMOUT
    INTEGER, PRIVATE :: NOUT = 0
    INTEGER, PARAMETER, PRIVATE :: MONUNIT = 12, DOMUNIT = 13
    INTEGER, DIMENSION(:), ALLOCATABLE :: IMONIT, JMONIT

    REAL :: TMONIT, TMONITOR = 0.0, TPRINT, TOUTPUT = 0.0

CONTAINS

    ! OUTPUT MONITORING POINT DATA
    SUBROUTINE OUTPUT_MONIT( INFILE, IMAX, JMAX, TIME, H, ETU, QXU, QYU, TAUX, TAUY )

        IMPLICIT NONE

        ! INPUT VARIABLES
        CHARACTER(*), INTENT(IN) :: INFILE
        INTEGER, INTENT(IN) :: IMAX, JMAX
        REAL, INTENT(IN) :: TIME
        REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: H, ETU, QXU, QYU, TAUX, TAUY

        ! LOCAL VARIABLES
        INTEGER :: N

        ! OPEN FILE AND OUTPUT HEADER
        IF( TMONITOR .LT. TMONIT ) THEN
            OPEN(UNIT=MONUNIT, FILE='out/'//TRIM(INFILE)//'_LST.dat', STATUS='UNKNOWN')
            WRITE(MONUNIT,'(A9,2X,100(A5,I4.2,5(A10,I3.2),5X))') 'TIME (S)', &
    &                                                            ('H',N,'ET',N,'QX',N,'QY',N,'TX',N,'TY',N, N=1,NMONIT)
        END IF

        ! UPDATE TIME AND OUTPUT DATA
        TMONITOR = TMONITOR + TMONIT
        WRITE(MONUNIT,'(ES11.4,100(1X,6ES13.4))') TIME, &
    &                                             ( H(IMONIT(N),JMONIT(N)), &
    &                                               ETU(IMONIT(N),JMONIT(N)), &
    &                                               QXU(IMONIT(N),JMONIT(N)), &
    &                                               QYU(IMONIT(N),JMONIT(N)), &
    &                                               TAUX(IMONIT(N),JMONIT(N)), &
    &                                               TAUY(IMONIT(N),JMONIT(N)), N=1,NMONIT )

    END SUBROUTINE OUTPUT_MONIT


    ! OUTPUT DATA FOR WHOLE DOMAIN
    SUBROUTINE OUTPUT_DOMAIN( INFILE, IMAX, JMAX, TIME, DX, DPMIN, IVEG, H, ETU, QXU, QYU, TAUX, TAUY )

        IMPLICIT NONE

        ! INPUT VARIABLES
        CHARACTER(*), INTENT(IN) :: INFILE
        INTEGER, INTENT(IN) :: IMAX, JMAX
        REAL, INTENT(IN) :: TIME, DX, DPMIN
        INTEGER, DIMENSION(IMAX,JMAX), INTENT(IN) :: IVEG
        REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: H, ETU, QXU, QYU, TAUX, TAUY

        ! LOCAL VARIABLES
        INTEGER :: I, J, N, NTVALS(8)

        ! OPEN FILE
        IF( TOUTPUT .LT. TPRINT ) THEN
            OPEN(UNIT=DOMUNIT, FILE='out/'//TRIM(INFILE)//'_UVE.dat', ACCESS='SEQUENTIAL')
        END IF

        ! UPDATE COUNTERS
        TOUTPUT = TOUTPUT + TPRINT
        NOUT = NOUT + 1

        ! OUTPUT CURRENT TIMESTEP INFO TO TERMINAL
        CALL DATE_AND_TIME(VALUES=NTVALS)
        IF( NOUT .EQ. 1 ) THEN
            WRITE(*,'(1X,4(A,3X))') 'CLOCK TIME','OUTPUT NO','TIME STEP','MODEL TIME'
            WRITE(*,'(1X,A)') REPEAT('-',47)
        END IF
        WRITE(*,'(1X,2(I2.2,":"),I2.2,5X,I3.3,A,I3.3,3X,I9,4X,ES9.3)') (NTVALS(N),N=5,7), NOUT, ' / ', NUMOUT, &
    &                                                                  NUMTSTEP, TIME
        IF( NOUT .EQ. NUMOUT ) THEN
            WRITE(*,'(1X,A)') REPEAT('-',47)
        END IF

        ! OUTPUT DOMAIN DATA TO FILE
        CALL SYSTEM_CLOCK( MSEC_STOP )
        WRITE(DOMUNIT,'(A,ES11.4,A,2(I8,A))') ' TIME: ', TIME, ' sec  TIME STEP: ', NUMTSTEP, &
    &                                         '  DURATION: ', MSEC_STOP-MSEC_START, ' msec'
        WRITE(DOMUNIT,*) 'IMAX: ', IMAX, '  JMAX: ', JMAX, '  DX: ', DX, '  DPMIN: ', DPMIN
        WRITE(DOMUNIT,*) 'VARIABLES: X (m), Y (m), H (m), E (m), QX (m2/s), QY (m2/s), TX (N/m2), TY (N/m2), IVEG'
        DO J = 1, JMAX
            DO I = 1, IMAX
                WRITE(DOMUNIT,'(8(ES11.4,1X),I2)') (I-1)*DX, (J-1)*DX, H(I,J), ETU(I,J), &
    &                                              QXU(I,J), QYU(I,J), TAUX(I,J), TAUY(I,J), IVEG(I,J)
            END DO
            WRITE(DOMUNIT,*)
        END DO
        WRITE(DOMUNIT,*)

    END SUBROUTINE OUTPUT_DOMAIN


    ! OUTPUT MAXIMUM VALUES OVER SIMULATION DURATION
    SUBROUTINE OUTPUT_MAXVALS( IMAX, JMAX, DX, DPMIN, IVEG, H, ETMAX, UVMAX, TAUMAX )

        IMPLICIT NONE

        ! INPUT VARIABLES
        INTEGER, INTENT(IN) :: IMAX, JMAX
        REAL, INTENT(IN) :: DX, DPMIN
        INTEGER, DIMENSION(IMAX,JMAX), INTENT(IN) :: IVEG
        REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: H, ETMAX, UVMAX, TAUMAX

        ! LOCAL VARIABLES
        INTEGER :: I, J

        ! OUTPUT MAX ELEVATION, VELOCITY AND BED SHEAR STRESS DATA
        WRITE(DOMUNIT,*) 'MAX VALUES DURING SIMULATION'
        WRITE(DOMUNIT,*) 'IMAX: ', IMAX, '  JMAX: ', JMAX, '  DX: ', DX, '  DPMIN: ', DPMIN
        WRITE(DOMUNIT,*) 'VARIABLES: X (m), Y (m), H (m), E (m), VMAG (m/s), TMAG (N/m2), IVEG'
        DO J = 1, JMAX
            DO I = 1, IMAX
                WRITE(DOMUNIT,'(6(ES11.4,1X),I2)') (I-1)*DX, (J-1)*DX, H(I,J), ETMAX(I,J), UVMAX(I,J), TAUMAX(I,J), IVEG(I,J)
            END DO
            WRITE(DOMUNIT,*)
        END DO

    END SUBROUTINE OUTPUT_MAXVALS


    ! DISPLAY PROGRAM DURATION
    SUBROUTINE OUTPUT_DURATION()

        IMPLICIT NONE

        ! LOCAL VARIABLES
        INTEGER :: ITOTAL, IHOUR, IMINUTE, ISEC

        ! CONVERT MILLISECONDS TO H:M:S
        CALL SYSTEM_CLOCK(MSEC_STOP)
        ITOTAL = MSEC_STOP - MSEC_START
        IHOUR = ITOTAL / (3600*1000)
        IMINUTE = (ITOTAL - 3600*1000*IHOUR) / (60*1000)
        ISEC = (ITOTAL - 3600*1000*IHOUR - 60*1000*IMINUTE) / 1000
        WRITE(*,'(/1X,A,I3,A,I2,A,I2,A//)') 'MODELLING COMPLETED IN: ', IHOUR, ' (h) ', IMINUTE, ' (m) ', ISEC, ' (s)'

    END SUBROUTINE OUTPUT_DURATION

END MODULE OUTPUT
