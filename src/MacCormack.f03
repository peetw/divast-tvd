MODULE MACCORMACK

    USE ADVECTION
    USE TURBULENCE
    USE ROUGHNESS
    USE VEGETATION

    IMPLICIT NONE

    REAL, DIMENSION(:,:), ALLOCATABLE, PRIVATE :: ETM, QXM, QYM

CONTAINS

    ! ------------------------------------------------------------------------------
    !
    !   STANDARD MACCORMACK SCHEME IN X DIRECTION
    !
    ! ------------------------------------------------------------------------------
    SUBROUTINE MACX( IMAX, JMAX, NFLCHZ, NFLEDV, DPMIN, DT, DX, CORI, BETA, EDCOEF, &
    &                IWET, IALL, IACT, VARCHZ, QM, H, ETL, QXL, QYL, ETU, QXU, QYU )

        IMPLICIT NONE

        ! INPUT VARIABLES
        INTEGER, INTENT(IN) :: IMAX, JMAX, NFLCHZ, NFLEDV
        REAL, INTENT(IN) :: DPMIN, DT, DX, CORI, BETA, EDCOEF
        LOGICAL(1), DIMENSION(IMAX,JMAX), INTENT(IN) :: IWET, IALL
        REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: VARCHZ, QM, H
        LOGICAL(1), DIMENSION(IMAX,JMAX), INTENT(INOUT) :: IACT
        REAL, DIMENSION(IMAX,JMAX), INTENT(INOUT) :: ETL, QXL, QYL, ETU, QXU, QYU

        ! LOCAL VARIABLES
        INTEGER :: I, J, IM1, IP1
        REAL :: TTMP, DTDX, DTDXSQ, HDT, HDTDXG, DTDXBETA, DTG, DTCORI, QXTMP, QYTMP, ETTMP
        ALLOCATE( DP(IMAX,JMAX), CHZ(IMAX,JMAX), EDV(IMAX,JMAX), VEGDRAG(IMAX,JMAX), &
    &             FX(IMAX,JMAX), FY(IMAX,JMAX), ETM(IMAX,JMAX), QXM(IMAX,JMAX), QYM(IMAX,JMAX) )

        ! DERIVED VALUES
        DTDX = DT / DX
        DTDXSQ = DTDX / DX
        HDT = 0.5 * DT
        HDTDXG = 0.5 * DTDX * 9.81
        DTDXBETA = DTDX * BETA
        DTG = DT * 9.81
        DTCORI = DT * CORI

        ! ------------ RESET MAIN VARIABLES ------------
        !$OMP PARALLEL
        !$OMP DO PRIVATE(I, J)
        DO J = 1, JMAX
            DO I = 1, IMAX
                IF( IALL(I,J) ) THEN
                    DP(I,J) = H(I,J) + ETU(I,J)
                    IF( DP(I,J) .GT. DPMIN ) THEN
                        IACT(I,J) = .TRUE.
                        QXL(I,J) = QXU(I,J)
                        QYL(I,J) = QYU(I,J)
                    ELSE
                        IACT(I,J) = .FALSE.
                        QXL(I,J) = 0.0
                        QYL(I,J) = 0.0
                    END IF
                    ETL(I,J) = ETU(I,J)
                END IF
            END DO
        END DO
        !$OMP END DO

        !$OMP DO PRIVATE(I, J)
        DO J = 1, JMAX
            DO I = 2, IMAX-1
                IF( IWET(I,J) .AND. IACT(I,J) ) THEN    !THE VELOCITY AT WET/DRY INTERFACE SHOULD BE 0
                    IF( (.NOT.IACT(I-1,J)) .OR. (.NOT.IACT(I+1,J)) ) THEN
                        QXL(I,J) = 0.0
                    END IF
                END IF
            END DO
        END DO
        !$OMP END DO
        !$OMP END PARALLEL

        CALL FUFV( IMAX, JMAX, IACT, QXL, QYL, DP )                          ! CALCULATE ADVECTION TERMS
        CALL DRAG( IMAX, JMAX, IWET, IACT, QXL, QYL, QXL, DP )               ! CALCULATE VEGETATIVE DRAG FORCE
        CALL CHEZY( IMAX, JMAX, NFLCHZ, IACT, VARCHZ, QXL, QYL )             ! CALCULATE CHEZY VALUE
        CALL EDDY( IMAX, JMAX, DX, NFLEDV, IACT, EDCOEF, QXL, QYL, CHZ, DP ) ! CALCULATE EDDY VISCOSITY

        ! ------------ PREDICTOR STEP ------------
        !$OMP PARALLEL
        !$OMP DO PRIVATE(I, J, IP1, TTMP, QXTMP, QYTMP) SCHEDULE(DYNAMIC, 10)
        DO J = 2, JMAX - 1
            DO I = 2, IMAX - 1
                IF( IWET(I,J) ) THEN
                    ! CONTINUITY EQUATION - X DIRECTION
                    IP1 = I + 1
                    ETM(I,J) = ETL(I,J) + (HDT*QM(I,J) - DTDX*(QXL(IP1,J)-QXL(I,J))) / VEGPOROS(I,J)

                    ! ACTIVE POINTS
                    IF( IACT(I,J) ) THEN
                        IM1 = I - 1
                        IF( IACT(IP1,J) .AND. IACT(IM1,J) )  THEN
                            ! ADVECTIVE ACCELERATION - X DIRECTION
                            QXTMP = QXL(I,J) - DTDXBETA*(FX(IP1,J)-FX(I,J))

                            ! CORIOLIS FORCE - X DIRECTION
                            QXTMP = QXTMP + DTCORI*QYL(I,J)

                            ! VEGATATIVE DRAG FORCE - X DIRECTION
                            QXTMP = QXTMP - DT*VEGDRAG(I,J)

                            ! PRESSURE GRADIENT (WATER SLOPE) - X DIRECTION
                            QXTMP = QXTMP - HDTDXG*(DP(I,J)+DP(IP1,J))*(ETL(IP1,J)-ETL(I,J))

                            ! BED FRICTION - X DIRECTION
                            TTMP = DTG * SQRT(QXL(I,J)**2 + QYL(I,J)**2) / (DP(I,J) * CHZ(I,J))**2

                            IF(TTMP .LT. 0.3) THEN
                                QXTMP = QXTMP - VEGPOROS(I,J)*TTMP*QXL(I,J) ! EXPLICIT SCHEME FOR BED FRICTION TERM
                            ELSE
                                QXTMP = QXTMP / (1.0 + VEGPOROS(I,J)*TTMP)  ! SEMI-IMPLICIT SCHEME FOR BED FRICTION TERM
                            END IF

                            ! TURBULENCE - X DIRECTION
                            QXM(I,J) = QXTMP + 2.0 * DTDXSQ * EDV(I,J) * (QXL(IP1,J)-2.0*QXL(I,J)+QXL(IM1,J))
                        ELSE
                            QXM(I,J) = 0.0
                        END IF

                        ! ADVECTIVE ACCELERATION - Y DIRECTION
                        QYTMP = QYL(I,J) - DTDXBETA * (FY(IP1,J)-FY(I,J))

                        ! TURBULENCE - Y DIRECTION
                        QYM(I,J) = QYTMP + DTDXSQ * EDV(I,J) * (QYL(IP1,J)-2.0*QYL(I,J)+QYL(IM1,J))
                    ELSE
                        ! INACTIVE POINTS
                        QXM(I,J) = QXL(I,J)
                        QYM(I,J) = QYL(I,J)
                    END IF
                END IF
            END DO
        END DO
        !$OMP END DO


        ! -------------- RESET MAIN VARIABLES --------------
        !$OMP DO PRIVATE(I, J)
        DO J = 1, JMAX
            DO I = 1, IMAX
                IF( IALL(I,J) ) THEN
                    IF( .NOT. IWET(I,J) ) THEN
                        ETM(I,J) = ETL(I,J)
                        QXM(I,J) = QXL(I,J)
                        QYM(I,J) = QYL(I,J)
                    END IF

                    DP(I,J) = H(I,J) + ETM(I,J)
                    IF( IACT(I,J) .AND. DP(I,J).LT.DPMIN ) THEN
                        IACT(I,J) = .FALSE.
                        QXM(I,J) = 0.0
                        QYM(I,J) = 0.0
                    END IF
                END IF
            END DO
        END DO
        !$OMP END DO

        !$OMP DO PRIVATE(I, J)
        DO J = 1, JMAX
            DO I = 2, IMAX-1
                IF( IWET(I,J) .AND. IACT(I,J) ) THEN    !THE VELOCITY AT WET/DRY INTERFACE SHOULD BE 0
                    IF( (.NOT.IACT(I-1,J)) .OR. (.NOT.IACT(I+1,J)) ) THEN
                        QXM(I,J) = 0.0
                    END IF
                END IF
            END DO
        END DO
        !$OMP END DO
        !$OMP END PARALLEL

        CALL FUFV( IMAX, JMAX, IACT, QXM, QYM, DP )                          ! CALCULATE ADVECTION TERMS
        CALL DRAG( IMAX, JMAX, IWET, IACT, QXM, QYM, QXM, DP )               ! CALCULATE VEGETATIVE DRAG FORCE
        CALL CHEZY( IMAX, JMAX, NFLCHZ, IACT, VARCHZ, QXM, QYM )             ! CALCULATE CHEZY VALUE
        CALL EDDY( IMAX, JMAX, DX, NFLEDV, IACT, EDCOEF, QXM, QYM, CHZ, DP ) ! CALCULATE EDDY VISCOSITY

        ! -------------- CORRECTOR STEP --------------
        !$OMP PARALLEL DO PRIVATE(I, J, IM1, TTMP, QXTMP, QYTMP, ETTMP) SCHEDULE(DYNAMIC, 10)
        DO J = 2, JMAX - 1
            DO I = 2, IMAX - 1
                IF( IWET(I,J) ) THEN
                    ! CONTINUITY EQUATION - X DIRECTION
                    IM1 = I - 1
                    ETTMP = ETL(I,J) + (HDT*QM(I,J) - DTDX*(QXM(I,J)-QXM(IM1,J))) / VEGPOROS(I,J)

                    ! ACTIVE POINTS
                    IF( IACT(I,J) ) THEN
                        IP1 = I + 1
                        IF( IACT(IM1,J) .AND. IACT(IP1,J) )  THEN
                            ! ADVECTIVE ACCELERATION - X DIRECTION
                            QXTMP = QXL(I,J) - DTDXBETA*(FX(I,J)-FX(IM1,J))

                            ! CORIOLIS FORCE - X DIRECTION
                            QXTMP = QXTMP + DTCORI*QYM(I,J)

                            ! VEGETATIVE DRAG FORCE - X DIRECTION
                            QXTMP = QXTMP - DT*VEGDRAG(I,J)

                            ! PRESSURE GRADIENT (WATER SLOPE) - X DIRECTION
                            QXTMP = QXTMP - HDTDXG*(DP(I,J)+DP(IM1,J))*(ETM(I,J)-ETM(IM1,J))

                            ! BED FRICTION - X DIRECTION
                            TTMP = DTG * SQRT(QXM(I,J)**2 + QYM(I,J)**2) / (DP(I,J) * CHZ(I,J))**2

                            IF(TTMP .LT. 0.3) THEN
                                QXTMP = QXTMP - VEGPOROS(I,J)*TTMP*QXM(I,J) ! EXPLICIT SCHEME FOR BED FRICTION TERM
                            ELSE
                                QXTMP = QXTMP / (1.0 + VEGPOROS(I,J)*TTMP)  ! SEMI-IMPLICIT SCHEME FOR BED FRICTION TERM
                            END IF

                            ! TURBULENCE - X DIRECTION
                            QXTMP = QXTMP + 2.0 * DTDXSQ * EDV(I,J) * (QXM(IP1,J)-2.0*QXM(I,J)+QXM(IM1,J))
                        ELSE
                            QXTMP = 0.0
                        END IF

                        ! ADVECTIVE ACCELERATION - Y DIRECTION
                        QYTMP = QYL(I,J) - DTDXBETA * (FY(I,J)-FY(IM1,J))

                        ! TURBULENCE - Y DIRECTION
                        QYTMP = QYTMP + DTDXSQ * EDV(I,J) * (QYM(IP1,J)-2.0*QYM(I,J)+QYM(IM1,J))

                        ! FINAL VALUE = 1/2 * (PREDICTOR + CORRECTOR)
                        ETU(I,J) = 0.5 * ( ETM(I,J) + ETTMP )
                        QXU(I,J) = 0.5 * ( QXM(I,J) + QXTMP )
                        QYU(I,J) = 0.5 * ( QYM(I,J) + QYTMP )
                    ELSE
                        ! INACTIVE POINTS
                        ETU(I,J) = 0.5 * ( ETM(I,J) + ETTMP )
                        QXU(I,J) = 0.5 * ( QXM(I,J) + QXL(I,J) )
                        QYU(I,J) = 0.5 * ( QYM(I,J) + QYL(I,J) )
                    END IF
                END IF
            END DO
        END DO
        !$OMP END PARALLEL DO

        DEALLOCATE( CHZ, EDV, DP, FX, FY, ETM, QXM, QYM, VEGDRAG )

    END SUBROUTINE MACX


    ! ------------------------------------------------------------------------------
    !
    !   STANDARD MACCORMACK SCHEME IN Y DIRECTION
    !
    ! ------------------------------------------------------------------------------
    SUBROUTINE MACY( IMAX, JMAX, NFLCHZ, NFLEDV, DPMIN, DT, DX, CORI, BETA, EDCOEF, &
    &                IWET, IALL, IACT, VARCHZ, QM, H, ETL, QXL, QYL, ETU, QXU, QYU )

        IMPLICIT NONE

        ! INPUT VARIABLES
        INTEGER, INTENT(IN) :: IMAX, JMAX, NFLCHZ, NFLEDV
        REAL, INTENT(IN) :: DPMIN, DT, DX, CORI, BETA, EDCOEF
        LOGICAL(1), DIMENSION(IMAX,JMAX), INTENT(IN) :: IWET, IALL
        REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: VARCHZ, QM, H
        LOGICAL(1), DIMENSION(IMAX,JMAX), INTENT(INOUT) :: IACT
        REAL, DIMENSION(IMAX,JMAX), INTENT(INOUT) :: ETL, QXL, QYL, ETU, QXU, QYU

        ! LOCAL VARIABLES
        INTEGER :: I, J, JM1, JP1
        REAL :: TTMP, DTDX, DTDXSQ, HDT, HDTDXG, DTDXBETA, DTG, DTCORI, QXTMP, QYTMP, ETTMP
        ALLOCATE( DP(IMAX,JMAX), CHZ(IMAX,JMAX), EDV(IMAX,JMAX), VEGDRAG(IMAX,JMAX), &
    &             FX(IMAX,JMAX), FY(IMAX,JMAX), ETM(IMAX,JMAX), QXM(IMAX,JMAX), QYM(IMAX,JMAX) )

        ! DERIVED VALUES
        DTDX = DT / DX
        DTDXSQ = DTDX / DX
        HDT = 0.5 * DT
        HDTDXG = 0.5 * DTDX * 9.81
        DTDXBETA = DTDX * BETA
        DTG = DT * 9.81
        DTCORI = DT * CORI

        ! ------------ RESET MAIN VARIABLES ------------
        !$OMP PARALLEL
        !$OMP DO PRIVATE(I, J)
        DO J = 1, JMAX
            DO I = 1, IMAX
                IF( IALL(I,J) ) THEN
                    DP(I,J) = H(I,J) + ETU(I,J)
                    IF( DP(I,J) .GT. DPMIN ) THEN
                        IACT(I,J) = .TRUE.
                        QXL(I,J) = QXU(I,J)
                        QYL(I,J) = QYU(I,J)
                    ELSE
                        IACT(I,J) = .FALSE.
                        QXL(I,J) = 0.0
                        QYL(I,J) = 0.0
                    END IF
                    ETL(I,J) = ETU(I,J)
                END IF
            END DO
        END DO
        !$OMP END DO

        !$OMP DO PRIVATE(I, J)
        DO J = 2, JMAX-1
            DO I = 1, IMAX
                IF( IWET(I,J) .AND. IACT(I,J) ) THEN    ! THE VELOCITY AT WET/DRY INTERFACE SHOULD BE 0
                    IF( (.NOT.IACT(I,J-1)) .OR. (.NOT.IACT(I,J+1)) ) THEN
                        QYL(I,J) = 0.0
                    END IF
                END IF
            END DO
        END DO
        !$OMP END DO
        !$OMP END PARALLEL

        CALL GUGV( IMAX, JMAX, IACT, QXL, QYL, DP )                          ! CALCULATE ADVECTION TERMS
        CALL DRAG( IMAX, JMAX, IWET, IACT, QXL, QYL, QYL, DP )               ! CALCULATE VEGETATIVE DRAG FORCE
        CALL CHEZY( IMAX, JMAX, NFLCHZ, IACT, VARCHZ, QXL, QYL )             ! CALCULATE CHEZY VALUE
        CALL EDDY( IMAX, JMAX, DX, NFLEDV, IACT, EDCOEF, QXL, QYL, CHZ, DP ) ! CALCULATE EDDY VISCOSITY

        ! ------------ PREDICTOR STEP ------------
        !$OMP PARALLEL
        !$OMP DO PRIVATE(I, J, JP1, TTMP, QXTMP, QYTMP) SCHEDULE(DYNAMIC, 10)
        DO J = 2, JMAX - 1
            JM1 = J - 1
            JP1 = J + 1
            DO I = 2, IMAX - 1
                IF( IWET(I,J) ) THEN
                    ! CONTINUITY EQUATION - Y DIRECTION
                    ETM(I,J) = ETL(I,J) + (HDT*QM(I,J) - DTDX*(QYL(I,JP1)-QYL(I,J))) / VEGPOROS(I,J)

                    ! ACTIVE POINTS
                    IF( IACT(I,J) ) THEN
                        IF( IACT(I,JP1) .AND. IACT(I,JM1) )  THEN
                            ! ADVECTIVE ACCELERATION - Y DIRECTION
                            QYTMP = QYL(I,J) - DTDXBETA*(FY(I,JP1)-FY(I,J))

                            ! CORIOLIS FORCE - Y DIRECTION
                            QYTMP = QYTMP - DTCORI*QXL(I,J)

                            ! VEGETATIVE DRAG FORCE - Y DIRECTION
                            QYTMP = QYTMP - DT*VEGDRAG(I,J)

                            ! PRESSURE GRADIENT (WATER SLOPE) - Y DIRECTION
                            QYTMP = QYTMP - HDTDXG*(DP(I,J)+DP(I,JP1))*(ETL(I,JP1)-ETL(I,J))

                            ! BED FRICTION - Y DIRECTION
                            TTMP = DTG * SQRT(QXL(I,J)**2 + QYL(I,J)**2) / (DP(I,J) * CHZ(I,J))**2

                            IF(TTMP .LT. 0.3) THEN
                                QYTMP = QYTMP - VEGPOROS(I,J)*TTMP*QYL(I,J) ! EXPLICIT SCHEME FOR BED FRICTION TERM
                            ELSE
                                QYTMP = QYTMP / (1.0 + VEGPOROS(I,J)*TTMP)  ! SEMI-IMPLICIT SCHEME FOR BED FRICTION TERM
                            END IF

                            ! TURBULENCE - Y DIRECTION
                            QYM(I,J) = QYTMP + 2.0 * DTDXSQ * EDV(I,J) * (QYL(I,JP1)-2.0*QYL(I,J)+QYL(I,JM1))
                        ELSE
                            QYM(I,J) = 0.0
                        END IF

                        ! ADVECTIVE ACCELERATION - X DIRECTION
                        QXTMP = QXL(I,J) - DTDXBETA * (FX(I,JP1)-FX(I,J))

                        ! TURBULENCE - X DIRECTION
                        QXM(I,J) = QXTMP + DTDXSQ * EDV(I,J) * (QXL(I,JP1)-2.0*QXL(I,J)+QXL(I,JM1))
                    ELSE
                        ! INACTIVE POINTS
                        QXM(I,J) = QXL(I,J)
                        QYM(I,J) = QYL(I,J)
                    END IF
                END IF
            END DO
        END DO
        !$OMP END DO


        ! ------------ RESET MAIN VARIABLES ------------
        !$OMP DO PRIVATE(I, J)
        DO J = 1, JMAX
            DO I = 1, IMAX
                IF( IALL(I,J) ) THEN
                    IF( .NOT. IWET(I,J) ) THEN
                        ETM(I,J) = ETL(I,J)
                        QXM(I,J) = QXL(I,J)
                        QYM(I,J) = QYL(I,J)
                    END IF
                    DP(I,J) = H(I,J) + ETM(I,J)
                    IF( IACT(I,J) .AND. DP(I,J).LT.DPMIN ) THEN
                        IACT(I,J) = .FALSE.
                        QXM(I,J) = 0.0
                        QYM(I,J) = 0.0
                    END IF
                END IF
            END DO
        END DO
        !$OMP END DO

        !$OMP DO PRIVATE(I, J)
        DO J = 2, JMAX-1
            DO I = 1, IMAX
                IF( IWET(I,J) .AND. IACT(I,J) ) THEN    ! THE VELOCITY AT WET/DRY INTERFACE SHOULD BE 0
                    IF( (.NOT.IACT(I,J-1)) .OR. (.NOT.IACT(I,J+1)) ) THEN
                        QYM(I,J) = 0.0
                    END IF
                END IF
            END DO
        END DO
        !$OMP END DO
        !$OMP END PARALLEL

        CALL GUGV( IMAX, JMAX, IACT, QXM, QYM, DP )                          ! CALCULATE ADVECTION TERMS
        CALL DRAG( IMAX, JMAX, IWET, IACT, QXM, QYM, QYM, DP )               ! CALCULATE VEGETATIVE DRAG FORCE
        CALL CHEZY( IMAX, JMAX, NFLCHZ, IACT, VARCHZ, QXM, QYM )             ! CALCULATE CHEZY VALUE
        CALL EDDY( IMAX, JMAX, DX, NFLEDV, IACT, EDCOEF, QXM, QYM, CHZ, DP ) ! CALCULATE EDDY VISCOSITY

        ! -------------- CORRECTOR STEP --------------
        !$OMP PARALLEL DO PRIVATE(I, J, JM1, TTMP, QXTMP, QYTMP, ETTMP) SCHEDULE(DYNAMIC, 10)
        DO J = 2, JMAX - 1
            JM1 = J - 1
            JP1 = J + 1
            DO I = 2, IMAX - 1
                IF( IWET(I,J) ) THEN
                    ! CONTINUITY EQUATION - Y DIRECTION
                    ETTMP = ETL(I,J) + (HDT*QM(I,J) - DTDX*(QYM(I,J)-QYM(I,JM1))) / VEGPOROS(I,J)

                    ! ACTIVE POINTS
                    IF( IACT(I,J) ) THEN
                        IF( IACT(I,JM1) .AND. IACT(I,JP1) )  THEN
                            ! ADVECTIVE ACCELERATION - Y DIRECTION
                            QYTMP = QYL(I,J) - DTDXBETA*(FY(I,J)-FY(I,JM1))

                            ! CORIOLIS FORCE - Y DIRECTION
                            QYTMP = QYTMP - DTCORI*QXM(I,J)

                            ! VEGETATIVE DRAG FORCE - Y DIRECTION
                            QYTMP = QYTMP - DT*VEGDRAG(I,J)

                            ! PRESSURE GRADIENT (WATER SLOPE) - Y DIRECTION
                            QYTMP = QYTMP - HDTDXG*(DP(I,J)+DP(I,JM1))*(ETM(I,J)-ETM(I,JM1))

                            ! BED FRICTION - Y DIRECTION
                            TTMP = DTG * SQRT(QXM(I,J)**2 + QYM(I,J)**2) / (DP(I,J) * CHZ(I,J))**2

                            IF(TTMP .LT. 0.3) THEN
                                QYTMP = QYTMP - VEGPOROS(I,J)*TTMP*QYM(I,J) ! EXPLICIT SCHEME FOR BED FRICTION TERM
                            ELSE
                                QYTMP = QYTMP / (1.0 + VEGPOROS(I,J)*TTMP)  ! SEMI-IMPLICIT SCHEME FOR BED FRICTION TERM
                            END IF

                            ! TURBULENCE - Y DIRECTION
                            QYTMP = QYTMP + 2.0 * DTDXSQ * EDV(I,J) * (QYM(I,JP1)-2.0*QYM(I,J)+QYM(I,JM1))
                        ELSE
                            QYTMP = 0.0
                        END IF

                        ! ADVECTIVE ACCELERATION - X DIRECTION
                        QXTMP = QXL(I,J) - DTDXBETA * (FX(I,J)-FX(I,JM1))

                        ! TURBULENCE - X DIRECTION
                        QXTMP = QXTMP + DTDXSQ * EDV(I,J) * (QXM(I,JP1)-2.0*QXM(I,J)+QXM(I,JM1))

                        ! FINAL VALUE = 1/2 * (PREDICTOR + CORRECTOR)
                        ETU(I,J) = 0.5 * ( ETM(I,J) + ETTMP )
                        QXU(I,J) = 0.5 * ( QXM(I,J) + QXTMP )
                        QYU(I,J) = 0.5 * ( QYM(I,J) + QYTMP )
                    ELSE
                        ! INACTIVE POINTS
                        ETU(I,J) = 0.5 * ( ETM(I,J) + ETTMP )
                        QXU(I,J) = 0.5 * ( QXM(I,J) + QXL(I,J) )
                        QYU(I,J) = 0.5 * ( QYM(I,J) + QYL(I,J) )
                    END IF
                END IF
            END DO
        END DO
        !$OMP END PARALLEL DO

        DEALLOCATE( CHZ, EDV, DP, FX, FY, ETM, QXM, QYM, VEGDRAG )

    END SUBROUTINE MACY

END MODULE MACCORMACK
