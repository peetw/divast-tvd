MODULE TURBULENCE

    IMPLICIT NONE

    REAL, DIMENSION(:,:), ALLOCATABLE :: EDV

    PRIVATE :: WALLDIST

CONTAINS

    ! ------------------------------------------------------------------------------
    !   GET DISTANCE TO NEAREST DRY CELL
    ! ------------------------------------------------------------------------------
    REAL FUNCTION WALLDIST( IPOS, JPOS, IMAX, JMAX, IACT, DX, TTMP )

        IMPLICIT NONE

        ! INPUT VARIABLES
        INTEGER, INTENT(IN) :: IPOS, JPOS, IMAX, JMAX
        LOGICAL(1), DIMENSION(IMAX,JMAX), INTENT(IN) :: IACT
        REAL, INTENT(IN) :: DX, TTMP

        ! LOCAL VARIABLES
        INTEGER :: I, J, IMTMP, IPTMP, JMTMP, JPTMP, N
        REAL :: WALLDISTTMP

        ! DETERMINE SEARCH AREA
        N = FLOOR(TTMP / DX)
        IMTMP = MAX(1, IPOS-N)
        IPTMP = MIN(IMAX, IPOS+N)
        JMTMP = MAX(1, JPOS-N)
        JPTMP = MIN(JMAX, JPOS+N)

        ! RETURN WATER DEPTH IF NO NEARBY CELLS ARE DRY
        WALLDIST = TTMP

        ! SEARCH FOR NEAREST DRY CELL
        DO J = JMTMP, JPTMP
            DO I = IMTMP, IPTMP
                IF( .NOT.IACT(I,J) ) THEN
                    WALLDISTTMP = DX * SQRT(REAL((IPOS-I)**2) + REAL((JPOS-J)**2))
                    IF( WALLDISTTMP .LT. WALLDIST) THEN
                        WALLDIST = WALLDISTTMP
                    END IF
                END IF
            END DO
        END DO

        RETURN

    END FUNCTION WALLDIST


    ! ------------------------------------------------------------------------------
    !   CALCULATE TURBULENT EDDY VISCOSITY
    !
    !   NFLEDV:
    !       0 - NO TURBULENT EDDY VISCOSITY
    !       1 - DEPTH AVERAGED EDDY VISCOSITY
    !       2 - MIXING LENGTH EDDY VISCOSITY
    !       3 - K-E MODEL EDDY VISCOSITY
    !
    !   NOTE: ASSUMES WATER VISCOSITY IS NEGLIGIBLE
    ! ------------------------------------------------------------------------------
    SUBROUTINE EDDY( IMAX, JMAX, DX, NFLEDV, IACT, EDCOEF, QX, QY, CHZ, DP )

        IMPLICIT NONE

        ! INPUT VARIABLES
        INTEGER, INTENT(IN) :: IMAX, JMAX, NFLEDV
        LOGICAL(1), DIMENSION(IMAX,JMAX), INTENT(IN) :: IACT
        REAL, INTENT(IN) :: DX, EDCOEF
        REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: QX, QY, CHZ, DP

        ! LOCAL VARIABLES
        INTEGER :: I, J
        REAL :: TTMP, USHEARSQ, LSQ, SSQ
        REAL, PARAMETER :: GSQRT = SQRT(9.81), KARMAN = 0.41, ALPHASQ = (KARMAN / 6.0)**2, CM = 1.2

        IF( NFLEDV .EQ. 1 ) THEN
            ! DEPTH AVERAGED EDDY VISCOSITY
            TTMP = EDCOEF * GSQRT
            !$OMP PARALLEL DO PRIVATE(I, J) SCHEDULE(DYNAMIC, 10)
            DO J = 2, JMAX-1
                DO I = 2, IMAX-1
                    IF( IACT(I,J) ) THEN
                        EDV(I,J) = TTMP * SQRT(QX(I,J)**2 + QY(I,J)**2) / CHZ(I,J)
                    END IF
                END DO
            END DO
            !$OMP END PARALLEL DO
        ELSE IF( NFLEDV .EQ. 2 ) THEN
            ! MIXING LENGTH EDDY VISCOSITY (SEE EQ.6 2004 WEIMING WU, PINGYI WANG, NOBUYUKI CHIBA)
            !$OMP PARALLEL DO PRIVATE(I, J, USHEARSQ, TTMP, LSQ, SSQ) SCHEDULE(DYNAMIC, 10)
            DO J = 2, JMAX-1
                DO I = 2, IMAX-1
                    IF( IACT(I,J) ) THEN
                        USHEARSQ = 9.81 * (QX(I,J)**2 + QY(I,J)**2) / CHZ(I,J)**2
                        TTMP = CM * DP(I,J)
                        IF( TTMP .GT. DX ) THEN
                            TTMP = WALLDIST(I, J, IMAX, JMAX, IACT, DX, TTMP)
                        END IF
                        LSQ = (KARMAN * TTMP)**2
                        SSQ = ( 0.5*(QX(I+1,J)-QX(I-1,J))**2 + 0.5*(QY(I,J+1)-QY(I,J-1))**2 + &
    &                          (0.5*(QX(I,J+1)-QX(I,J-1)+QY(I+1,J)-QY(I-1,J)))**2 ) / DX**2
                        EDV(I,J) = SQRT(ALPHASQ * USHEARSQ + LSQ**2 * SSQ)
                    END IF
                END DO
            END DO
            !$OMP END PARALLEL DO
        ELSE IF( NFLEDV .EQ. 3 ) THEN
            ! K-E MODEL EDDY VISCOSITY

        ELSE
            ! NO TURBULENT DIFFUSION
            EDV = 0.0
        END IF

    END SUBROUTINE EDDY

END MODULE TURBULENCE
