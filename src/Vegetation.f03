MODULE VEGETATION

    IMPLICIT NONE

    INTEGER, DIMENSION(:), ALLOCATABLE :: NPOINTS
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: IVEG
    REAL, DIMENSION(:), ALLOCATABLE :: VEGDEN, HEIGHT, CD, EI, VOGEL
    REAL, DIMENSION(:,:), ALLOCATABLE :: DPVAR, APVAR, WDVAR, VEGDRAG, VEGPOROS

    PRIVATE :: INTERP

CONTAINS

    REAL FUNCTION INTERP( N, DP, VAR )

        IMPLICIT NONE

        INTEGER, INTENT(IN) :: N
        REAL, INTENT(IN) :: DP
        REAL, DIMENSION(:,:), INTENT(IN) :: VAR

        ! LOCAL VARIABLES
        INTEGER :: I, IP1

        DO I = 1, NPOINTS(N)-1
            IP1 = I + 1
            IF( DP.GE.DPVAR(N,I) .AND. DP.LE.DPVAR(N,IP1) ) THEN
                INTERP = VAR(N,I) + (VAR(N,IP1)-VAR(N,I)) * (DP-DPVAR(N,I)) / (DPVAR(N,IP1)-DPVAR(N,I))
                RETURN
            END IF
        END DO

        WRITE(*,'(/1X,A/)') 'ERROR IN FUNCTION VEGETATION::INTERP'
        STOP 1

    END FUNCTION INTERP


    SUBROUTINE DRAG( IMAX, JMAX, IWET, IACT, QX, QY, Q, DP )

        IMPLICIT NONE

        ! INPUT VARIABLES
        INTEGER, INTENT(IN) :: IMAX, JMAX
        LOGICAL(1), DIMENSION(IMAX,JMAX), INTENT(IN) :: IWET, IACT
        REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: QX, QY, Q, DP

        ! LOCAL VARIABLES
        INTEGER :: I, J, N
        REAL :: AP, CDAP, CY, HT, VMAG, WIDTH, WDAVRG
        REAL, PARAMETER :: RHO = 1000.0, PI4 = 3.1415926 / 4.0

        ! CALCULATE DRAG FORCE
        !$OMP PARALLEL DO PRIVATE(I, J, N, AP, HT, WIDTH, WDAVRG, VMAG, CY, CDAP) SCHEDULE(DYNAMIC, 10)
        DO J = 1, JMAX
            DO I = 1, IMAX
                IF( IACT(I,J) .AND. IWET(I,J) ) THEN
                    ! VEGETATION TYPE
                    N = IVEG(I,J)

                    IF( VEGDEN(N) .GT. 0.0 ) THEN
                        ! CHECK IF VEGETATION IS SUBMERGED
                        IF( DP(I,J) .GT. HEIGHT(N) ) THEN
                            AP = APVAR(N,NPOINTS(N))
                            HT = HEIGHT(N)
                            WIDTH = WDVAR(N,NPOINTS(N))
                        ELSE
                            AP = INTERP(N, DP(I,J), APVAR)
                            HT = DP(I,J)
                            WIDTH = INTERP(N, DP(I,J), WDVAR)
                        END IF

                        ! CALCULATE BLOCKAGE EFFECT DUE TO VEGETATION
                        WDAVRG = 0.5 * (WIDTH + WDVAR(N,1))
                        VEGPOROS(I,J) = MAX(1.0E-15, 1.0 - PI4 * WDAVRG**2 * VEGDEN(N))

                        ! CALCULATE CAUCHY NUMBER
                        VMAG = SQRT(QX(I,J)**2 + QY(I,J)**2) / DP(I,J)
                        CY = MAX(1.0, RHO * VMAG**2 * AP * HT * WIDTH / EI(N))

                        ! CALCULATE DEPTH AVERAGED DRAG FORCE IN X OR Y DIRECTION
                        CDAP = CD(N) * AP * CY**(0.5*VOGEL(N))
                        VEGDRAG(I,J) = 0.5 * CDAP * VEGDEN(N) * VMAG * Q(I,J) / DP(I,J)
                    ELSE
                        VEGPOROS(I,J) = 1.0
                        VEGDRAG(I,J) = 0.0
                    END IF
                ELSE
                    VEGPOROS(I,J) = 1.0
                END IF
            END DO
        END DO
        !$OMP END PARALLEL DO

    END SUBROUTINE DRAG

END MODULE VEGETATION
