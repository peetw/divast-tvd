!>     DR. DONGFANG LIANG, PROF. R.A. FALCONER, DR. BINLIANG LIN
!!     SCHOOL OF ENGINEERING,
!!     CARDIFF UNIVERSITY
!!
!!     MATHEMATICAL MODEL "DIVAST_TVD": - (C) COPYRIGHT 2005-2006
!!     (DIVAST - DEPTH INTEGRATED VELOCITIES AND SOLUTE TRANSPORT)
!!     (TVD - TOTAL VARIATION DIMINISHING)
!!
!! ---------------------------------------------------------------------------------------------
!!        LIST OF MAJOR VARIABLES (ALL IN THE UNIT OF M, S, KG)
!!
!!        IMAX      :MAX NO. OF GRID POINTS IN X - DIRECTION
!!        JMAX      :MAX NO. OF GRID POINTS IN Y - DIRECTION
!!        IWET      :DOMAIN DESCRIPTION (THE OUTER EDGES HAVE TO BE ZERO)
!!              1/T -WATER WHERE THE COMPUTATION SHOULD BE CARRIED OUT
!!              0/F -LAND OR BOUNDARY WHERE THE NORMAL COMPUTATION IS NOT NEEDED
!!
!! ---------------------------------------------------------------------------------------------

!  TODO: Add input checks (DPMIN > k/12, veg properties > 0.0, etc)

PROGRAM DIVAST_TVD

        !$ USE OMP_LIB      ! OPENMP FUNCTIONS
        USE INPUT           ! READ INPUT DATA
        USE BOUNDARY        ! BOUNDARY CONDITIONS
        USE MACCORMACK      ! MAIN HYDRODYNAMICS
        USE TVD             ! TVD STEP
        USE OUTPUT          ! OUTPUT DATA

        IMPLICIT NONE

        ! MAIN VARIABLES
        LOGICAL(1) :: ALTER, ADAPT
        LOGICAL(1), DIMENSION(:,:), ALLOCATABLE :: IWET, IALL, IACT

        INTEGER :: I, J, IMAX, JMAX, NFLALT, NFLCHZ, NFLEDV, NFLTVD, NTHREADS

        REAL :: DX, DT, DTMIN, DTMAX, DPMIN, ANGLAT, CORI, CRANT, BETA, EDCOEF, &
        &       TIME, TIMESM, TTMP, VELMAX
        REAL, DIMENSION(:,:), ALLOCATABLE :: VARCHZ, H, ETL, ETMAX, QXL, QXU, QYL, QYU, UVMAX, QM
        REAL, DIMENSION(:,:), ALLOCATABLE, TARGET :: ETU
        REAL, POINTER :: PTR


        ! GET INPUT FILENAME FROM COMMAND LINE
        IF( COMMAND_ARGUMENT_COUNT() .EQ. 1 ) THEN
            CALL GET_COMMAND_ARGUMENT(1, INFILE)
        ELSE
            CALL GET_COMMAND_ARGUMENT(0, INFILE)    ! GET PROGRAM EXECUTABLE NAME
            WRITE(*,'(/1X,A,A,A/)') 'USAGE: ', TRIM(INFILE), ' <INPUT_FILENAME>'
            STOP 1
        END IF

        ! BEGIN TIMING
        CALL SYSTEM_CLOCK( MSEC_START )


! ------------------------------------------------------------------------------
!     READ IN PARAMETERS
! ------------------------------------------------------------------------------

        ! OPEN INPUT FILES
        CALL READ_FILENAMES()

        ! READ IN CONTROL PARAMETERS
        CALL READ_PARAMS( IMAX, JMAX, NFLTVD, NFLALT, NFLCHZ, NFLEDV, &
    &                     DPMIN, TIMESM, TPRINT, &
    &                     DTMIN, DTMAX, CRANT, DX, ANGLAT, BETA, EDCOEF )

        ! READ IN TIME SERIES DATA, E.G. TIDAL CURVE
        CALL READ_TIDAL()

        ! READ BOUNDARY CONDITIONS
        CALL READ_BOUNDARY()

        ! READ IN OBSERVATION POINTS
        CALL READ_MONIT( NMONIT, TMONIT, IMONIT, JMONIT )

        ! READ IN DOMAIN DESCRIPTION
        CALL READ_DOMAIN( IMAX, JMAX, IWET, IALL, IVEG )

        ! READ IN HEIGHTS BELOW PRESCRIBED DATUM (BED LEVEL), ABOVE DATUM AND DISCHARGES (INITIAL VALUES)
        CALL READ_INITIAL( IMAX, JMAX, DPMIN, IALL, IACT, H, ETU, QXU, QYU, VARCHZ, QM, ETMAX, UVMAX )

        ! READ IN VEGETATION PROPERTIES
        CALL READ_VEG( NPOINTS, VEGDEN, HEIGHT, CD, EI, VOGEL, DPVAR, APVAR, WDVAR )

        ! ALLOCATE AND INITIALIZE ARRAYS
        ALLOCATE( ETL(IMAX,JMAX), QXL(IMAX,JMAX), QYL(IMAX,JMAX), &
        &         TAUX(IMAX,JMAX), TAUY(IMAX,JMAX), TAUMAX(IMAX,JMAX), VEGPOROS(IMAX,JMAX) )
        TAUX = 0.0
        TAUY = 0.0
        TAUMAX = 0.0

        ! CALCULATE DERIVED VALUES
        CORI = 2.0 * (2.0*3.1415926 / (60.0*60.0*24.0)) * SIN(ANGLAT * 3.1415926 / 180.0)

        ! INITIALIZE TIME STEPPING VARIABLES
        TIME = 0.0
        NUMOUT = INT(TIMESM/TPRINT) + 1
        ALTER = .FALSE.
        IF( ABS(DTMAX-DTMIN) .LT. 0.00001 ) THEN
            ADAPT = .FALSE.
            DT = DTMIN
        ELSE
            ADAPT = .TRUE.
        END IF

        ! OUTPUT NUMBER OF THREADS IN USE
        !$OMP PARALLEL
            !$OMP MASTER
            NTHREADS = 1
            !$ NTHREADS = omp_get_num_threads()
            WRITE(*,'(/1X,A,I2,A/)') 'COMMENCING HYDRODYNAMIC MODELLING USING', NTHREADS, ' THREADS...'
            !$OMP END MASTER
        !$OMP END PARALLEL

! ==============================================================================
!
!       COMMENCE TIME MARCHING
!
! ==============================================================================
        DO WHILE( TIME .LT. TIMESM )

        ! DETERMINE TIME STEP
        IF( ADAPT ) THEN
            VELMAX = 0.0
            !$OMP PARALLEL DO PRIVATE(I, J, TTMP) REDUCTION(max : VELMAX)
            DO J = 1, JMAX
                DO I = 1, IMAX
                    IF( IACT(I,J) ) THEN
                        TTMP = H(I,J) + ETU(I,J)
                        TTMP = SQRT(9.81*TTMP) + SQRT(QXU(I,J)**2 + QYU(I,J)**2) / TTMP
                        VELMAX = MERGE(TTMP, VELMAX, TTMP .GT. VELMAX)
                    END IF
                END DO
            END DO
            !$OMP END PARALLEL DO

            DT = CRANT * DX / ( 1.0E-10 + VELMAX )
            IF( DT .LT. DTMIN ) THEN
                DT = DTMIN
            ELSE IF( DT .GT. DTMAX ) THEN
                DT = DTMAX
            END IF
        END IF

        ! CURRENT TIME AND TIME STEP
        NUMTSTEP = NUMTSTEP + 1
        TIME = TIME + DT

        ! WETTING JUDGEMENT
        DO J = 2, JMAX-1
            DO I = 2, IMAX-1
                IF( IWET(I,J) .AND. (.NOT.IACT(I,J)) )  THEN
                    ! SEARCH FOR THE NEIGHBOURING WET CELL WITH HIGHEST WATER LEVEL
                    PTR => ETU(I,J)
                    IF( IACT(I+1,J) .AND. ETU(I+1,J).GT.PTR ) THEN
                        PTR => ETU(I+1,J)
                    END IF
                    IF( IACT(I-1,J) .AND. ETU(I-1,J).GT.PTR ) THEN
                        PTR => ETU(I-1,J)
                    END IF
                    IF( IACT(I,J+1) .AND. ETU(I,J+1).GT.PTR ) THEN
                        PTR => ETU(I,J+1)
                    END IF
                    IF( IACT(I,J-1) .AND. ETU(I,J-1).GT.PTR ) THEN
                        PTR => ETU(I,J-1)
                    END IF

                    IF( (PTR-ETU(I,J)) .GT. DPMIN*2.0 ) THEN
                        ETU(I,J) = ETU(I,J) + DPMIN
                        PTR = PTR - DPMIN
                    END IF
                END IF
            END DO
        END DO

        ! ALTERNATE THE OPERATORS
        IF( NFLALT .GT. 0 ) THEN
            ALTER = .NOT. ALTER
        END IF

        ! MAIN HYDRODYNAMIC CALCULATIONS
        IF( ALTER ) THEN
            CALL HYDRO_CALC_Y()
            CALL HYDRO_CALC_X()
        ELSE
            CALL HYDRO_CALC_X()
            CALL HYDRO_CALC_Y()
        END IF

        ! LABEL ALL THE ACTIVE CELLS AND RECORD MAXIMUM WATER LEVELS AND VELOCITIES
        !$OMP PARALLEL DO PRIVATE(I, J, TTMP, VELMAX)
        DO J = 1, JMAX
            DO I = 1, IMAX
                TTMP = H(I,J) + ETU(I,J)
                IACT(I,J) = MERGE(.TRUE., .FALSE., TTMP .GT. DPMIN)
                ETMAX(I,J) = MERGE(ETU(I,J), ETMAX(I,J), ETU(I,J) .GT. ETMAX(I,J))
                IF( IACT(I,J) ) THEN
                    VELMAX = SQRT(QXU(I,J)**2 + QYU(I,J)**2) / TTMP
                    UVMAX(I,J) = MERGE(VELMAX, UVMAX(I,J), VELMAX .GT. UVMAX(I,J))
                END IF
            END DO
        END DO
        !$OMP END PARALLEL DO

        ! CALCULATE BED SHEAR STRESSES
        CALL BED_SHEAR( IMAX, JMAX, NFLCHZ, IACT, H, ETU, QXU, QYU, VARCHZ, VEGPOROS )

        ! OUTPUT MONITORING POINT DATA
        IF( NMONIT .GT. 0 .AND. TIME .GE. TMONITOR ) THEN
            CALL OUTPUT_MONIT( INFILE, IMAX, JMAX, TIME, H, ETU, QXU, QYU, TAUX, TAUY )
        END IF

        ! OUTPUT DATA FOR WHOLE DOMAIN
        IF( TIME .GE. TOUTPUT ) THEN
            CALL OUTPUT_DOMAIN( INFILE, IMAX, JMAX, TIME, DX, DPMIN, IVEG, H, ETU, QXU, QYU, TAUX, TAUY )
        END IF
! ==============================================================================
!
!       END OF TIME MARCHING
!
! ==============================================================================
        END DO


        ! OUTPUT MAXIMUM VALUES OVER SIMULATION DURATION
        CALL OUTPUT_MAXVALS( IMAX, JMAX, DX, DPMIN, IVEG, H, ETMAX, UVMAX, TAUMAX )

        ! DISPLAY PROGRAM DURATION
        CALL OUTPUT_DURATION()


! ------------------------------------------------------------------------------
!     HELPER FUNCTIONS
! ------------------------------------------------------------------------------
CONTAINS

        ! WRAPPER TO CALL X DIRECTION HYDRODYNAMIC CALCULATIONS
        SUBROUTINE HYDRO_CALC_X()
            CALL HYDBND( IMAX, JMAX, TIME, DX, DPMIN, H, ETU, QXU, QYU, QM )

            CALL MACX( IMAX, JMAX, NFLCHZ, NFLEDV, DPMIN, DT, DX, CORI, BETA, EDCOEF, &
    &                  IWET, IALL, IACT, VARCHZ, QM, H, ETL, QXL, QYL, ETU, QXU, QYU )

            IF(NFLTVD .GT. 0) THEN
                CALL TVDX( IMAX, JMAX, IWET, IACT, H, ETL, QXL, QYL, ETU, QXU, QYU, DT/DX )
            END IF
        END SUBROUTINE HYDRO_CALC_X

        ! WRAPPER TO CALL Y DIRECTION HYDRODYNAMIC CALCULATIONS
        SUBROUTINE HYDRO_CALC_Y()
            CALL HYDBND( IMAX, JMAX, TIME, DX, DPMIN, H, ETU, QXU, QYU, QM )

            CALL MACY( IMAX, JMAX, NFLCHZ, NFLEDV, DPMIN, DT, DX, CORI, BETA, EDCOEF, &
    &                  IWET, IALL, IACT, VARCHZ, QM, H, ETL, QXL, QYL, ETU, QXU, QYU )

            IF(NFLTVD .GT. 0) THEN
                CALL TVDY( IMAX, JMAX, IWET, IACT, H, ETL, QXL, QYL, ETU, QXU, QYU, DT/DX )
            END IF
        END SUBROUTINE HYDRO_CALC_Y

END PROGRAM DIVAST_TVD
