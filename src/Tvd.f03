MODULE TVD

    IMPLICIT NONE

    REAL, DIMENSION(:,:), ALLOCATABLE, PRIVATE :: DETM1, DQXM1, DQYM1, GPLUS, GMINUS

CONTAINS

    SUBROUTINE TVDX( IMAX, JMAX, IWET, IACT, H, ETL, QXL, QYL, ETU, QXU, QYU, DTDX )

        IMPLICIT NONE

        INTEGER, INTENT(IN) :: IMAX, JMAX
        LOGICAL(1), DIMENSION(IMAX,JMAX),INTENT(IN) :: IWET, IACT
        REAL, INTENT(IN) :: DTDX, H(IMAX,JMAX)
        REAL, DIMENSION(IMAX,JMAX), INTENT(INOUT) :: ETL, QXL, QYL, ETU, QXU, QYU

        ! LOCAL VARIABLES
        INTEGER :: I, J, IM1, IP1
        REAL :: RPLUS, RMINUS, TTMP, DPTMP, CR
        ALLOCATE( DETM1(IMAX,JMAX),DQXM1(IMAX,JMAX),DQYM1(IMAX,JMAX),GPLUS(IMAX,JMAX),GMINUS(IMAX,JMAX) )

        ! LOCAL INCREMENTS OF THE UNKNOWN VARIABLES
        !$OMP PARALLEL
        !$OMP DO PRIVATE(I, J, IM1)
        DO J = 2, JMAX - 1
            DO I = 2, IMAX
                IM1 = I - 1
                IF( IACT(I,J) .AND. IACT(IM1,J) )  THEN
                    DETM1(I,J) = ETL(I,J) - ETL(IM1,J)
                    DQXM1(I,J) = QXL(I,J) - QXL(IM1,J)
                    DQYM1(I,J) = QYL(I,J) - QYL(IM1,J)
                END IF
            END DO
        END DO
        !$OMP END DO

        ! THE SUCCESSIVE GRADIENT RATIO AND G
        !$OMP DO PRIVATE(I, J, IM1, IP1, RPLUS, RMINUS, TTMP, DPTMP, CR)
        DO J = 2, JMAX - 1
            DO I = 1, IMAX
                IF( IACT(I,J) ) THEN
                    IM1 = I - 1
                    IP1 = I + 1
                    IF( I.EQ.1 .OR. I.EQ.IMAX .OR. .NOT.IACT(IM1,J) .OR. .NOT.IACT(IP1,J) ) THEN
                        GMINUS(I,J) = 0.0
                        GPLUS(I,J) = 0.0
                    ELSE
                        ! NUMERATOR FOR RPLUS & RMINUS CALCULATIONS
                        TTMP = DETM1(I,J)*DETM1(IP1,J) + DQXM1(I,J)*DQXM1(IP1,J) + DQYM1(I,J)*DQYM1(IP1,J)

                        ! LOCAL COURANT NUMBER
                        DPTMP = H(I,J) + ETL(I,J)
                        CR = DTDX * (ABS(QXL(I,J)/DPTMP) + SQRT(9.81*DPTMP))
                        IF(CR .LT. 0.5) THEN
                            CR = 0.5 * CR * (1.0-CR)
                        ELSE
                            CR = 0.125
                        END IF

                        ! GMINUS
                        IF( I .EQ. 2 )  THEN
                            GMINUS(I,J) = 0.0
                        ELSE
                            RMINUS = TTMP / (1.0E-15 + DETM1(I,J)**2 + DQXM1(I,J)**2 + DQYM1(I,J)**2)
                            GMINUS(I,J) = CR * (1.0 - MAX(0.0, MIN(2.0*RMINUS, 1.0)))
                        END IF

                        ! GPLUS
                        IF( I .EQ. IMAX-1 )  THEN
                            GPLUS(I,J) = 0.0
                        ELSE
                            RPLUS = TTMP / (1.0E-15 + DETM1(IP1,J)**2 + DQXM1(IP1,J)**2 + DQYM1(IP1,J)**2)
                            GPLUS(I,J) = CR * (1.0 - MAX(0.0, MIN(2.0*RPLUS, 1.0)))
                        END IF
                    END IF
                END IF
            END DO
        END DO
        !$OMP END DO

        ! EXTRA DIFFUSION FLUX
        !$OMP DO PRIVATE(I, J, IM1, TTMP)
        DO J = 2, JMAX-1
            DO I = 2, IMAX
                IM1 = I - 1
                IF( IACT(IM1,J) .AND. IACT(I,J) )  THEN
                    TTMP = GPLUS(IM1,J) + GMINUS(I,J)
                    ETL(I,J) = TTMP * DETM1(I,J)
                    QXL(I,J) = TTMP * DQXM1(I,J)
                    QYL(I,J) = TTMP * DQYM1(I,J)
                ELSE
                    ETL(I,J) = 0.0
                    QXL(I,J) = 0.0
                    QYL(I,J) = 0.0
                END IF
            END DO
        END DO
        !$OMP END DO

        !$OMP DO PRIVATE(I, J, IP1)
        DO J = 2, JMAX - 1
            DO I = 2, IMAX - 1
                IF( IWET(I,J) .AND. IACT(I,J) ) THEN
                    IP1 = I + 1
                    ETU(I,J) = ETU(I,J) + ETL(IP1,J) - ETL(I,J)
                    QXU(I,J) = QXU(I,J) + QXL(IP1,J) - QXL(I,J)
                    QYU(I,J) = QYU(I,J) + QYL(IP1,J) - QYL(I,J)
                END IF
            END DO
        END DO
        !$OMP END DO
        !$OMP END PARALLEL

        DEALLOCATE( DETM1, DQXM1, DQYM1, GPLUS, GMINUS )

    END SUBROUTINE TVDX




    SUBROUTINE TVDY( IMAX, JMAX, IWET, IACT, H, ETL, QXL, QYL, ETU, QXU, QYU, DTDX )

        IMPLICIT NONE

        INTEGER, INTENT(IN) :: IMAX, JMAX
        LOGICAL(1), DIMENSION(IMAX,JMAX),INTENT(IN) :: IWET, IACT
        REAL, INTENT(IN) :: DTDX, H(IMAX,JMAX)
        REAL, DIMENSION(IMAX,JMAX), INTENT(INOUT) :: ETL, QXL, QYL, ETU, QXU, QYU

        ! LOCAL VARIABLES
        INTEGER :: I, J, JM1, JP1
        REAL :: RPLUS, RMINUS, TTMP, DPTMP, CR
        ALLOCATE( DETM1(IMAX,JMAX),DQXM1(IMAX,JMAX),DQYM1(IMAX,JMAX),GPLUS(IMAX,JMAX),GMINUS(IMAX,JMAX) )

        ! LOCAL INCREMENTS OF THE UNKNOWN VARIABLES
        !$OMP PARALLEL
        !$OMP DO PRIVATE(I, J, JM1)
        DO J = 2, JMAX
            JM1 = J - 1
            DO I = 2, IMAX - 1
                IF( IACT(I,J) .AND. IACT(I,JM1) )  THEN
                    DETM1(I,J) = ETL(I,J) - ETL(I,JM1)
                    DQXM1(I,J) = QXL(I,J) - QXL(I,JM1)
                    DQYM1(I,J) = QYL(I,J) - QYL(I,JM1)
                END IF
            END DO
        END DO
        !$OMP END DO

        ! THE SUCCESSIVE GRADIENT RATIO AND G
        !$OMP DO PRIVATE(I, J, JM1, JP1, RPLUS, RMINUS, TTMP, DPTMP, CR)
        DO J = 1, JMAX
            JM1 = J - 1
            JP1 = J + 1
            DO I = 2, IMAX - 1
                IF( IACT(I,J) ) THEN
                    IF( J.EQ.1 .OR. J.EQ.JMAX .OR. .NOT.IACT(I,JM1) .OR. .NOT.IACT(I,JP1) ) THEN
                        GMINUS(I,J) = 0.0
                        GPLUS(I,J) = 0.0
                    ELSE
                        ! NUMERATOR FOR RPLUS & RMINUS CALCULATIONS
                        TTMP = DETM1(I,J)*DETM1(I,JP1) + DQXM1(I,J)*DQXM1(I,JP1) + DQYM1(I,J)*DQYM1(I,JP1)

                        ! LOCAL COURANT NUMBER
                        DPTMP = H(I,J) + ETL(I,J)
                        CR = DTDX * (ABS(QYL(I,J)/DPTMP) + SQRT(9.81*DPTMP))
                        IF(CR .LT. 0.5) THEN
                            CR = 0.5 * CR * (1.0-CR)
                        ELSE
                            CR = 0.125
                        END IF

                        ! GMINUS
                        IF( J .EQ. 2 )  THEN
                            GMINUS(I,J) = 0.0
                        ELSE
                            RMINUS = TTMP / (1.0E-15 + DETM1(I,J)**2 + DQXM1(I,J)**2 + DQYM1(I,J)**2)
                            GMINUS(I,J) = CR * (1.0 - MAX(0.0, MIN(2.0*RMINUS, 1.0)))
                        END IF

                        ! GPLUS
                        IF( J .EQ. JMAX-1 )  THEN
                            GPLUS(I,J) = 0.0
                        ELSE
                            RPLUS = TTMP / (1.0E-15 + DETM1(I,JP1)**2 + DQXM1(I,JP1)**2 + DQYM1(I,JP1)**2)
                            GPLUS(I,J) = CR * (1.0 - MAX(0.0, MIN(2.0*RPLUS, 1.0)))
                        END IF
                    END IF
                END IF
            END DO
        END DO
        !$OMP END DO

        ! EXTRA DIFFUSION FLUX
        !$OMP DO PRIVATE(I, J, JM1, TTMP)
        DO J = 2, JMAX
            JM1 = J - 1
            DO I = 2, IMAX - 1
                IF( IACT(I,JM1) .AND. IACT(I,J) ) THEN
                    TTMP = GPLUS(I,JM1) + GMINUS(I,J)
                    ETL(I,J) = TTMP * DETM1(I,J)
                    QXL(I,J) = TTMP * DQXM1(I,J)
                    QYL(I,J) = TTMP * DQYM1(I,J)
                ELSE
                    ETL(I,J) = 0.0
                    QXL(I,J) = 0.0
                    QYL(I,J) = 0.0
                END IF
            END DO
        END DO
        !$OMP END DO

        !$OMP DO PRIVATE(I, J, JP1)
        DO J = 2, JMAX - 1
            DO I = 2, IMAX - 1
                IF( IWET(I,J) .AND. IACT(I,J) ) THEN
                    JP1 = J + 1
                    ETU(I,J) = ETU(I,J) + ETL(I,JP1) - ETL(I,J)
                    QXU(I,J) = QXU(I,J) + QXL(I,JP1) - QXL(I,J)
                    QYU(I,J) = QYU(I,J) + QYL(I,JP1) - QYL(I,J)
                END IF
            END DO
        END DO
        !$OMP END DO
        !$OMP END PARALLEL

        DEALLOCATE( DETM1, DQXM1, DQYM1, GPLUS, GMINUS )

    END SUBROUTINE TVDY

END MODULE TVD
