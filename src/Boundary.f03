MODULE BOUNDARY

    IMPLICIT NONE

    INTEGER :: NET1, NET2, NET3, NET4, NET8, NET9, &
    &          NQX1, NQX2, NQX8, NQX9, NQY1, NQY2, NQY8, NQY9, &
    &          NQM9, NCUL, NDATASET
    INTEGER, DIMENSION(:), ALLOCATABLE :: IET1MIN,IET1MAX,JET1MIN,JET1MAX, &
    &                                     IET2MIN,IET2MAX,JET2MIN,JET2MAX,IET2REF,JET2REF, &
    &                                     IET3MIN,IET3MAX,JET3MIN,JET3MAX,IET3REF,JET3REF, &
    &                                     IET4MIN,IET4MAX,JET4MIN,JET4MAX, &
    &                                     IET8MIN,IET8MAX,JET8MIN,JET8MAX,IET8REF,JET8REF, &
    &                                     IET9MIN,IET9MAX,JET9MIN,JET9MAX,NET9DATA, &
    &                                     IQX1MIN,IQX1MAX,JQX1MIN,JQX1MAX, &
    &                                     IQX2MIN,IQX2MAX,JQX2MIN,JQX2MAX,IQX2REF,JQX2REF, &
    &                                     IQX8MIN,IQX8MAX,JQX8MIN,JQX8MAX,IQX8REF,JQX8REF, &
    &                                     IQX9MIN,IQX9MAX,JQX9MIN,JQX9MAX,NQX9DATA, &
    &                                     IQY1MIN,IQY1MAX,JQY1MIN,JQY1MAX, &
    &                                     IQY2MIN,IQY2MAX,JQY2MIN,JQY2MAX,IQY2REF,JQY2REF, &
    &                                     IQY8MIN,IQY8MAX,JQY8MIN,JQY8MAX,IQY8REF,JQY8REF, &
    &                                     IQY9MIN,IQY9MAX,JQY9MIN,JQY9MAX,NQY9DATA, &
    &                                     IQM9MIN,IQM9MAX,JQM9MIN,JQM9MAX,NQM9DATA, &
    &                                     IQMCUL1,JQMCUL1,IQMCUL2,JQMCUL2, &
    &                                     NUMDATA

    REAL, DIMENSION(:), ALLOCATABLE :: ET10, ET1M, ET1T, ET4WEIR, QX1, QY1, CULLOSS, CULAREA
    REAL, DIMENSION(:,:), ALLOCATABLE :: TDATA, VDATA

    PRIVATE :: INTERP

CONTAINS

! ------------------------------------------------------------------------------
!
!       THIS SUBROUTINE INTERPOLATES THE TIDAL LEVEL AT A DESIRED TIME
!       ACCORDING TO GIVEN TIDAL CURVE
!
! ------------------------------------------------------------------------------
    REAL FUNCTION INTERP( N, TIME )

        IMPLICIT NONE

        INTEGER, INTENT(IN) :: N
        REAL, INTENT(IN) :: TIME

        ! LOCAL VARIABLES
        INTEGER :: I, IP1

        DO I = 1, NUMDATA(N)-1
            IP1 = I+1
            IF( TIME.GE.TDATA(N,I) .AND. TIME.LE.TDATA(N,IP1) ) THEN
                INTERP = VDATA(N,I) + (VDATA(N,IP1)-VDATA(N,I)) * (TIME-TDATA(N,I)) / (TDATA(N,IP1)-TDATA(N,I))
                RETURN
            END IF
        END DO

        WRITE(*,'(2(/1X,A),/)') 'ERROR IN FUNCTION BOUNDARY::INTERP','SIMULATION TIME EXCEEDED MAX INPUT HYDYOGRAPH TIME'
        STOP 1

    END FUNCTION INTERP

! ------------------------------------------------------------------------------
!
!>  SUBROUTINE HYDBND INTRODUCES OPEN HYDRODYNAMIC BOUNDARY CONDITION
!!  INPUT: DX, DPMIN - USED BY CULVERT TYPE OF BC
!!
!!  OUTPUT: ET,QX,QY,QM
!!
!! ------------------------------------------------------------------------------
    SUBROUTINE HYDBND( IMAX, JMAX, TIME, DX, DPMIN, H, ET, QX, QY, QM )

        IMPLICIT NONE

        INTEGER, INTENT(IN) :: IMAX, JMAX
        REAL, INTENT(IN) :: TIME, DX, DPMIN, H(IMAX,JMAX)
        REAL, DIMENSION(IMAX,JMAX), INTENT(INOUT) :: ET, QX, QY, QM

        ! LOCAL VARIABLES
        INTEGER :: N, I, J, ITMP, JTMP
        REAL :: TTMP
        REAL, PARAMETER :: CW = 1.5 / SQRT(19.62)

        ! TIDAL WATER LEVEL
        DO N = 1, NET1
            TTMP = ET10(N) + ET1M(N) * SIN(6.2832*TIME/ET1T(N))
            DO I = IET1MIN(N), IET1MAX(N)
                DO J = JET1MIN(N), JET1MAX(N)
                    ET(I,J) = TTMP
                END DO
            END DO
        END DO

        ! ZERO GRADIENT WATER LEVEL
        DO N = 1, NET2
            ITMP = IET2REF(N)
            JTMP = JET2REF(N)
            DO J = JET2MIN(N), JET2MAX(N)
                DO I = IET2MIN(N), IET2MAX(N)
                    ET(I,J) = ET(I+ITMP,J+JTMP)
                END DO
            END DO
        END DO

        ! ZERO GRADIENT WATER DEPTH
        DO N = 1, NET3
            ITMP = IET3REF(N)
            JTMP = JET3REF(N)
            DO J = JET3MIN(N), JET3MAX(N)
                DO I = IET3MIN(N), IET3MAX(N)
                    ET(I,J) = ET(I+ITMP,J+JTMP) + H(I+ITMP,J+JTMP) - H(I,J)
                END DO
            END DO
        END DO

        ! 2ND DERIVATIVE OF WATER LEVEL = 0
        DO N = 1, NET8
            ITMP = IET8REF(N)
            JTMP = JET8REF(N)
            DO J = JET8MIN(N), JET8MAX(N)
                DO I = IET8MIN(N), IET8MAX(N)
                    ET(I,J) = 2.0 * ET(I+ITMP,J+JTMP) - ET(I+ITMP+ITMP,J+JTMP+JTMP)
                END DO
            END DO
        END DO

        ! WATER LEVEL TIME SERIES
        DO N = 1, NET9
            TTMP = INTERP(NET9DATA(N),TIME)
            DO I = IET9MIN(N), IET9MAX(N)
                DO J = JET9MIN(N), JET9MAX(N)
                    ET(I,J) = TTMP
                END DO
            END DO
        END DO

        ! QX FIXED
        DO N = 1, NQX1
            TTMP = QX1(N)
            DO I = IQX1MIN(N), IQX1MAX(N)
                DO J = JQX1MIN(N), JQX1MAX(N)
                    QX(I,J) = TTMP
                END DO
            END DO
        END DO

        ! ZERO GRADIENT QX
        DO N = 1, NQX2
            ITMP = IQX2REF(N)
            JTMP = JQX2REF(N)
            DO J = JQX2MIN(N), JQX2MAX(N)
                DO I = IQX2MIN(N), IQX2MAX(N)
                    QX(I,J) = QX(I+ITMP,J+JTMP)
                END DO
            END DO
        END DO

        ! 2ND DERIVATIVE OF QX = 0
        DO N = 1, NQX8
            ITMP = IQX8REF(N)
            JTMP = JQX8REF(N)
            DO J = JQX8MIN(N), JQX8MAX(N)
                DO I = IQX8MIN(N), IQX8MAX(N)
                    QX(I,J) = 2.0 * QX(I+ITMP,J+JTMP) - QX(I+ITMP+ITMP,J+JTMP+JTMP)
                END DO
            END DO
        END DO

        ! QX TIME SERIES
        DO N = 1, NQX9
            TTMP = INTERP(NQX9DATA(N),TIME)
            DO I = IQX9MIN(N), IQX9MAX(N)
                DO J = JQX9MIN(N), JQX9MAX(N)
                    QX(I,J) = TTMP
                END DO
            END DO
        END DO

        ! QY FIXED
        DO N = 1, NQY1
            TTMP = QY1(N)
            DO I = IQY1MIN(N), IQY1MAX(N)
                DO J = JQY1MIN(N), JQY1MAX(N)
                    QY(I,J) = TTMP
                END DO
            END DO
        END DO

        ! ZERO GRADIENT QY
        DO N = 1, NQY2
            ITMP = IQY2REF(N)
            JTMP = JQY2REF(N)
            DO J = JQY2MIN(N), JQY2MAX(N)
                DO I = IQY2MIN(N), IQY2MAX(N)
                    QY(I,J) = QY(I+ITMP,J+JTMP)
                END DO
            END DO
        END DO

        ! 2ND DERIVATIVE OF QY = 0
        DO N = 1, NQY8
            ITMP = IQY8REF(N)
            JTMP = JQY8REF(N)
            DO J = JQY8MIN(N), JQY8MAX(N)
                DO I = IQY8MIN(N), IQY8MAX(N)
                    QY(I,J) = 2.0 * QY(I+ITMP,J+JTMP) - QY(I+ITMP+ITMP,J+JTMP+JTMP)
                END DO
            END DO
        END DO

        ! QY TIME SERIES
        DO N = 1, NQY9
            TTMP = INTERP(NQY9DATA(N),TIME)
            DO I = IQY9MIN(N), IQY9MAX(N)
                DO J = JQY9MIN(N), JQY9MAX(N)
                    QY(I,J) = TTMP
                END DO
            END DO
        END DO

        ! IT IS IMPORTANT TO IMPLEMENT THE WEIR FORMULA AFTER INTRODUCING THE VELOCITY B.C.
        DO N = 1, NET4
            DO J = JET4MIN(N), JET4MAX(N)
                DO I = IET4MIN(N), IET4MAX(N)
                    TTMP = (CW * SQRT(QX(I,J)**2 + QY(I,J)**2) / ET4WEIR(N))**0.66667
                    ET(I,J) = TTMP - H(I,J)
                END DO
            END DO
        END DO

        ! VERTICAL FLOWS
        DO N = 1, NQM9
            TTMP = INTERP(NQM9DATA(N),TIME)
            DO I = IQM9MIN(N), IQM9MAX(N)
                DO J = JQM9MIN(N), JQM9MAX(N)
                    QM(I,J) = TTMP
                END DO
            END DO
        END DO

        ! CULVERTS
        DO N = 1, NCUL
            TTMP = ET(IQMCUL1(N),JQMCUL1(N)) - ET(IQMCUL2(N),JQMCUL2(N))
            IF( TTMP.GT.DPMIN .AND. H(IQMCUL1(N),JQMCUL1(N))+ET(IQMCUL1(N),JQMCUL1(N)).GT.DPMIN ) THEN
                ! FLOW FROM CELL 1 TO CELL 2
                TTMP = CULAREA(N) * SQRT( 19.62*TTMP/CULLOSS(N) ) / DX**2
                QM(IQMCUL1(N),JQMCUL1(N)) = -TTMP
                QM(IQMCUL2(N),JQMCUL2(N)) = TTMP
            ELSE IF( (-TTMP).GT.DPMIN .AND. H(IQMCUL2(N),JQMCUL2(N))+ET(IQMCUL2(N),JQMCUL2(N)).GT.DPMIN ) THEN
                ! FLOW FROM CELL 2 TO CELL 1
                TTMP = CULAREA(N) * SQRT( 19.62*(-TTMP)/CULLOSS(N) ) / DX**2
                QM(IQMCUL2(N),JQMCUL2(N)) = -TTMP
                QM(IQMCUL1(N),JQMCUL1(N)) = TTMP
            ELSE
                QM(IQMCUL1(N),JQMCUL1(N)) = 0.0
                QM(IQMCUL2(N),JQMCUL2(N)) = 0.0
            END IF
        END DO

    END SUBROUTINE HYDBND

END MODULE BOUNDARY
