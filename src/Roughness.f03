MODULE ROUGHNESS

    IMPLICIT NONE

    REAL, DIMENSION(:,:), ALLOCATABLE :: CHZ, DP, TAUX, TAUY, TAUMAX

CONTAINS

    ! CALCULATE BED ROUGHNESS VALUES
    SUBROUTINE CHEZY(IMAX, JMAX, NFLCHZ, IACT, VARCHZ, QX, QY)

        IMPLICIT NONE

        ! INPUT VARIABLES
        INTEGER, INTENT(IN) :: IMAX, JMAX, NFLCHZ
        LOGICAL(1), DIMENSION(IMAX,JMAX), INTENT(IN) :: IACT
        REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: VARCHZ, QX, QY

        ! LOCAL VARIABLES
        INTEGER :: I, J
        REAL :: RE, TTMP, CHZTMP, CHZOLD
        REAL, PARAMETER :: CPARAM = 2.0 * SQRT(8.0 * 9.81)

        ! =========================================
        ! See pages 44-45 of ENT722 Section 1 notes
        ! =========================================

        IF( NFLCHZ .EQ. 1 ) THEN
            ! CONSTANT VALUE
            DO J = 2, JMAX - 1
            DO I = 2, IMAX - 1
                IF( IACT(I,J) ) THEN
                    CHZ(I,J) = VARCHZ(I,J)     ! C has units m^(1/2)/s
                END IF
            END DO
            END DO
        ELSE IF( NFLCHZ .EQ. 2 ) THEN
            ! MANNING FORMULA
            DO J = 2, JMAX - 1
            DO I = 2, IMAX - 1
                IF( IACT(I,J) ) THEN
                    CHZ(I,J) = DP(I,J)**0.166667 / VARCHZ(I,J) ! VARCHZ = Manning's n
                END IF
            END DO
            END DO
        ELSE IF( NFLCHZ .EQ. 3 ) THEN
            ! COLEBROOK-WHITE FORMULA (Transitional flow, i.e. Re ~ 500-2000)
            !$OMP PARALLEL DO PRIVATE(I, J, RE, TTMP, CHZTMP, CHZOLD) SCHEDULE(DYNAMIC, 10)
            DO J = 2, JMAX - 1
            DO I = 2, IMAX - 1
                IF( IACT(I,J) ) THEN
                    ! Re = U*L/v = U*Dh/v = (U*4A/P)/v = (U*4*dx*DP/dx)/v = 4q/v
                    RE = 4.0 * SQRT(QX(I,J)**2 + QY(I,J)**2) / 1.30E-6
                    RE = MERGE(500.0, RE, RE .LT. 500.0)

                    TTMP = VARCHZ(I,J) / (12.0*DP(I,J))                     ! VARCHZ = k = roughness height
                    CHZOLD = -CPARAM * LOG10(TTMP)                          ! C = -18*log10(k/12DP)
                    CHZTMP = -CPARAM * LOG10(TTMP + 5.0*CHZOLD/(RE*CPARAM)) ! C = -18*log10(k/12DP + 5C/18Re)

                    DO WHILE( ABS(CHZTMP-CHZOLD) .GT. 0.5 )
                        CHZOLD = CHZTMP
                        CHZTMP = -CPARAM * LOG10(TTMP + 5.0*CHZOLD/(RE*CPARAM))
                    END DO
                    CHZ(I,J) = CHZTMP
                END IF
            END DO
            END DO
            !$OMP END PARALLEL DO
        ! NOTE: Chezy is always greater when using NFL4 vs NFL3
        ELSE IF( NFLCHZ .EQ. 4 ) THEN
            ! COLEBROOK-WHITE FORMULA (Fully rough flow i.e. Re >> 1000, section 2.9 DIVAST user man
            !$OMP PARALLEL DO PRIVATE(I, J) SCHEDULE(DYNAMIC, 10)
            DO J = 2, JMAX - 1
            DO I = 2, IMAX - 1
                IF( IACT(I,J) ) THEN  ! varchz = k = roughness height
                    CHZ(I,J) = -CPARAM * LOG10(VARCHZ(I,J) / (12.0*DP(I,J)))  ! C = -18*log10(k/12DP)
                    ! IMPORTANT: DP > k/12 otherwise C will be negative (applies to NFL3 as well)
                END IF
            END DO
            END DO
            !$OMP END PARALLEL DO
        END IF
    END SUBROUTINE CHEZY


    ! CALCULATE BED SHEAR STRESS IN N/M^2
    SUBROUTINE BED_SHEAR( IMAX, JMAX, NFLCHZ, IACT, H, ETU, QXU, QYU, VARCHZ, VEGPOROS )

        IMPLICIT NONE

        INTEGER, INTENT(IN) :: IMAX, JMAX, NFLCHZ
        LOGICAL(1), DIMENSION(IMAX,JMAX), INTENT(IN) :: IACT
        REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: H, ETU, QXU, QYU, VARCHZ, VEGPOROS

        ! LOCAL VARIABLES
        INTEGER :: I, J
        REAL :: TMP, TAU
        REAL, PARAMETER :: RHOG = 1000.0 * 9.81
        ALLOCATE( CHZ(IMAX,JMAX), DP(IMAX,JMAX) )

        ! CALCULATE WATER DEPTH
        DP = H + ETU

        ! CALCULATE CHEZY VALUES
        CALL CHEZY( IMAX, JMAX, NFLCHZ, IACT, VARCHZ, QXU, QYU )

        ! CALCULATE BED SHEAR STRESS (SEE ENT722 NOTES SECTION 1, PAGE 44)
        !$OMP PARALLEL DO PRIVATE(I, J, TMP, TAU) SCHEDULE(DYNAMIC, 10)
        DO J = 2, JMAX - 1
        DO I = 2, IMAX - 1
            IF( IACT(I,J) ) THEN

                TMP = VEGPOROS(I,J) * RHOG * SQRT(QXU(I,J)**2 + QYU(I,J)**2) / (DP(I,J)*CHZ(I,J))**2

                TAUX(I,J) = QXU(I,J) * TMP
                TAUY(I,J) = QYU(I,J) * TMP

                TAU = SQRT(TAUX(I,J)**2 + TAUY(I,J)**2)

                ! UPDATE MAX STRESS
                TAUMAX(I,J) = MERGE(TAU, TAUMAX(I,J), TAU .GT. TAUMAX(I,J))
            ELSE
                TAUX(I,J) = 0.0
                TAUY(I,J) = 0.0
            END IF
        END DO
        END DO
        !$OMP END PARALLEL DO

        DEALLOCATE( CHZ, DP )

    END SUBROUTINE BED_SHEAR


    ! CONVERT FROM MANNING'S N TO EQUIVALENT ROUGHNESS HEIGHT, KS
    SUBROUTINE CONVERT_NK_STRICKLER( IMAX, JMAX, VARCHZ )

        IMPLICIT NONE

        ! INPUT VARIABLES
        INTEGER, INTENT(IN) :: IMAX, JMAX
        REAL, DIMENSION(IMAX,JMAX), INTENT(INOUT) :: VARCHZ

        ! LOCAL VARIABLES
        INTEGER :: I, J

        ! These approaches are only really applicable where H / ks > ~10
        ! i.e. not applicable in shallow water with small relative roughness like floodplains
        ! Also note, ks and grain sizes are sometimes interchanged in the literature (D50=median grain size f.ex.)

        ! Strickler eq (see p47 of strickler 1923)
        ! Reference: Strickler, A., 1923, "Contributions to the question of a velocity formula and
        !            roughness data for streams",
        !            Translation by T. Roesgen and W. Brownlie, California Institute of Technology, Jan., 1981
        ! Alternative reference: Jaeger 1956, p.30
        ! Lots of variations of this exist where 21.1 is replaced by a different value
        ! Brownlie 1983 "Flow Depth in Sand-Bed Channels" proposed n = ks^1/6 / 8.1*g^1/2 = 0.0394*k^1/6
        ! Chow 1959 "Open Channel Hydraulics" proposed n = 0.034*ks^1/6 where ks is in ft?
        DO J = 1, JMAX
        DO I = 1, IMAX
            VARCHZ(I,J) = ( 21.1*VARCHZ(I,J) )**6
        END DO
        END DO
    END SUBROUTINE CONVERT_NK_STRICKLER


    ! CONVERT FROM MANNING'S N TO EQUIVALENT ROUGHNESS HEIGHT, KS
    SUBROUTINE CONVERT_NK_KEULEGAN( IMAX, JMAX, VARCHZ, KS )

        IMPLICIT NONE

        INTEGER, INTENT(IN) :: IMAX, JMAX
        REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: VARCHZ
        REAL, DIMENSION(IMAX,JMAX), INTENT(OUT) :: KS

        ! LOCAL VARIABLES
        INTEGER :: I, J
        REAL :: TMP

        ! These approaches are only really applicable where H / ks > ~10
        ! i.e. not applicable in shallow water with small relative roughness like floodplains
        ! Also note, ks and grain sizes are sometimes interchanged in the literature (D50=median grain size f.ex.)

        ! Relative roughness approach, i.e. roughness varies with depth
        ! Reference: Keulegan, G., "Laws of turbulent flow in open channels",
        !            Research Paper 1151, National Bureau of Standards, Vol. 21 No. 6 (1938)
        ! The following equation is taken from River2D model by Uni. of Alberta (http://tinyurl.com/763wam2)
        DO J = 1, JMAX
        DO I = 1, IMAX
            TMP = DP(I,J)**0.166667 / (2.5*VARCHZ(I,J)*SQRT(9.81))
            TMP = EXP(TMP)
            KS(I,J) = 12.0*DP(I,J) / TMP
        END DO
        END DO
    END SUBROUTINE CONVERT_NK_KEULEGAN

END MODULE ROUGHNESS
