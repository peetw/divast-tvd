# Main directories
BINDIR = bin
DOCDIR = doc
OBJDIR = obj
MODDIR = mod
SRCDIR = src

# Directories to create if not already existant
MKDIR := $(OBJDIR) $(MODDIR) $(BINDIR)

# Binary file
BIN := $(BINDIR)/divast-tvd

# Fortran object files
FSRC := $(wildcard $(SRCDIR)/*.f03)
FOBJ := $(patsubst %.f03,%.o,$(subst $(SRCDIR),$(OBJDIR),$(FSRC)))

# Dependency file
DEP = .depend

# Doxygen config and output files
DOXY_SRC = .doxyfile
DOXY_HTML := $(DOCDIR)/html/index.html

# Compiler options
FC = gfortran
FFLAGS := -c -O3 -J $(MODDIR) -std=f2003 -pedantic-errors -Wall -Wextra -Warray-temporaries -Werror \
		  -mtune=native -ffpe-trap=invalid,zero,overflow -fimplicit-none -fopenmp

# Linker options
LDFLAGS = -fopenmp
LDLIBS =

# NON-FILE TARGETS
.PHONY: all debug doc profile serial precise clean

# DEFAULT TARGET
all: $(MKDIR) $(BIN)

# DEBUG TARGET
debug: FFLAGS := -g $(filter-out -O%, $(FFLAGS))
debug: FFLAGS := $(filter-out -fopenmp, $(FFLAGS))
debug: LDFLAGS := -g $(filter-out -O%, $(LDFLAGS))
debug: LDFLAGS := $(filter-out -fopenmp, $(LDFLAGS))
debug: all

# PROFILING TARGET
profile: FC = scalasca -instrument gfortran
profile: all

# SERIAL TARGET
serial: FFLAGS := $(filter-out -fopenmp, $(FFLAGS))
serial: LDFLAGS := $(filter-out -fopenmp, $(LDFLAGS))
serial: all

# PRECISE TARGET
precise: FFLAGS += -fdefault-real-8
precise: all

# DOXYGEN TARGET
doc: $(DOXY_HTML)

# CREATE DOXYGEN DOCS
$(DOXY_HTML): $(FSRC) $(DOXY_SRC)
	mkdir -p $(DOCDIR)
	doxygen $(DOXY_SRC)

# CREATE DIRS
$(MKDIR):
	mkdir -p $@

# CREATE BINARY
$(BIN): $(FOBJ)
	@$(RM) $(BIN)
	$(FC) $(LDFLAGS) $^ $(LDLIBS) -o $@
	@echo "\n**** Compiled on: "`date`" ****"

# DETERMINE DEPENDICES
$(DEP): $(FSRC)
	@makedepf90 $^ > $(DEP)
	@sed -i 's|$(SRCDIR)/\([^ ]*\.o\)|$(OBJDIR)/\1|g' $(DEP)

-include $(DEP)

# COMPILE
$(OBJDIR)/%.o: $(SRCDIR)/%.f03
	$(FC) $(FFLAGS) $< -o $@

# CLEAN PROJECT
clean:
	$(RM) $(BIN)
	$(RM) $(DEP)
	$(RM) $(FOBJ)
	$(RM) $(MODDIR)/*
